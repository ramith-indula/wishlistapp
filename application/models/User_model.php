<?php

class User_model extends CI_Model
{
    public function __construct()
    {
        $this->load->database();
    }

    //API call - add new list
    public function add($data)
    {
        if ($this->db->insert('wishlist', $data)) {
            return true;
        } else {
            return false;
        }
    }

    //API call - get all lists
    public function getAllItems($userId)
    {
        $this->db->select('id, listName, listDescription, userId');

        $this->db->where('userId', $userId);

        $this->db->from('wishlist');

        $query = $this->db->get();

        if ($query->num_rows() > 0) {

            return $query->result_array();

        } else {

            return false;
        }

    }

    //Login
    public function can_login($username, $password)
    {
        //$this->db->where(array('userName' => $username, 'password' => hash('sha256', $password)));
        $this->db->where('userName', $username);
        $this->db->where('password', $password);
        $query = $this->db->get('users');

        if ($query->num_rows() > 0) {

            return $query->result_array();
        } else {
            return false;
        }
    }

    // Register new user
    public function registerUser($username, $email, $password)
    {
        $this->db->set('userName', $username);
        $this->db->set('password', $password);
        $this->db->set('email', $email);
        //$this->db->set('password', hash('sha256', $password));
        $this->db->insert('users');
        return $this->db->insert_id();

    }

    public function getListIdOfUser($data){

        $query = $this->db->get_where('wishlist', $data);
        return $query->result_array();
    }

    public function addWishList($userId,$wishListName,$wishListDes){
        $this->db->set('userId', $userId);
        $this->db->set('listName', $wishListName);
        $this->db->set('listDescription', $wishListDes);
        $this->db->insert('wishlist');
        return $this->db->insert_id();
    }
}