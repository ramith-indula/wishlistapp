<?php
class items_model extends CI_Model
{

    public function __construct()
    {
        $this->load->database();
    }

    //API call - get all items
    public function getAllItems($listId)
    {
        $this->db->select('id, itemPrice, itemName, itemDescription, priorityLevel, occasion, itemUrl');

        $this->db->from('listitems');


        $this->db->where('listId', $listId);

        $query = $this->db->get();

        if ($query->num_rows() > 0) {

            return $query->result_array();

        } else {

            return 0;

        }

    }

    //API call - delete an Item
    public function deleteItem($id)

    {
        $this->db->where('id', $id);

        if ($this->db->delete('listitems')) {

            return true;

        } else {

            return false;

        }

    }

    //API call - add new
    public function add($data)
    {
        if ($this->db->insert('listitems', $data)) {

            return $this->db->insert_id();

        } else {

            return false;

        }

    }

    //API call - update an item
    public function updateItem($id, $data)
    {
        $this->db->where('id', $id);

        if ($this->db->update('listitems', $data)) {

            return true;

        } else {

            return false;

        }

    }

}