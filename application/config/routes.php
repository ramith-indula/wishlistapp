<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$route['dashboard']='user_controller/dashboard';
$route['share/(:num)']='user_controller/shareList';
$route['share']='user_controller/shareList';
$route['login']='user_controller/login';
$route['TheWishList2']='user_controller/login';
$route['default_controller'] = 'user_controller/login';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;
