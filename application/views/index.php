<!DOCTYPE html>
<html lang="en">
<head>
	<link rel="stylesheet" href="application\views\css\bootstrap.min.css">
	<meta charset="UTF-8">
</head>
<body>
	<div class="container">
		<table class="table">
			<thead>
				<tr>
				</tr>
				<tr>
					<td><input class="form-control itemName-input" placeholder="Item name"></td>
					<td><input class="form-control itemDescription-input" placeholder="Description"></td>
					<td><input class="form-control priorityLevel-input" placeholder="Priority Level"></td>
					<td><input class="form-control occasion-input" placeholder="Occasion"></td>
					<td><input class="form-control itemUrl-input" placeholder="URL"></td>
					<td><button class="btn btn-primary add-item" >Add</button></td>
				</tr>
			</thead>
			<tbody class="items-list"></tbody>
		</table>
	</div>
	<script type="text/template" class="items-list-template">
	<td><span class="itemName"><%= itemName %></span></td>
	<td><span class="itemDescription"><%= itemDescription %></span></td>
	<td><span class="priorityLevel"><%= priorityLevel %></span></td>
	<td><span class="occasion"><%= occasion %></span></td>
	<td><span class="itemUrl"><%= itemUrl %></span></td>
	<td><button class="btn btn-warning edit-item">Edit</button> <button class="btn btn-danger delete-item">Delete</button><button class="btn btn-success update-item" style="display:none">Update</button> <button class="btn btn-danger cancel" style="display:none">Cancel</button></td>

	</script>

    <script type="text/javascript" src="application\views\js\jquery-3.3.1.min.js"></script>
	<script src="application\views\js\underscore-min.js"></script>
	<script src="application\views\js\backbone-min.js"></script>
	<script src="application\views\js\scripts.js"></script>
</body>
</html>