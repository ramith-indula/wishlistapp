<!--Main layout-->
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>Wish List</title>
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css">
    <!-- Bootstrap core CSS -->
    <link href="<?php echo base_url('application/views/css/bootstrap.min.css'); ?>" rel="stylesheet">
    <!-- Material Design Bootstrap -->
    <link href="<?php echo base_url('application/views/css/mdb.min.css'); ?>" rel="stylesheet">
    <!-- Your custom styles (optional) -->
    <link href="<?php echo base_url('application/views/css/style.css'); ?>" rel="stylesheet">
</head>


    <div class="container-fluid mt-5">

        <!-- Heading -->
        <div class="card mb-4 wow fadeIn">

            <!--Card content-->
            <div class="card-body d-sm-flex justify-content-between">

                <h4 style="text-align: center">
                    <a  id="wishlistname"  target="_blank"></a>
                </h4>

            </div>

        </div>
        <!-- Heading -->

        <!--Grid row-->

        <!--Grid column-->
        <div class="card mb-4 wow fadeIn table-items">

            <!--Card-->
            <div class="card-body d-sm-flex justify-content-between">

                <!--Card content-->
                <div class="card-body">

                    <!-- Table  -->
                    <table class="table table-hover">
                        <!-- Table head -->
                        <thead class="blue-grey lighten-4">
                        <tr>
                            <th>Item Name</th>
                            <th>Price</th>
                            <th>Description</th>
                            <th>Priority Level</th>
                            <th>Occasion</th>
                            <th>URL</th>
                        </tr>
                        </thead>
                        <!-- Table head -->

                        <!-- Table body -->
                        <tbody class="items-list"></tbody>
                        <!-- Table body -->
                    </table>
                    <!-- Table  -->

                </div>

            </div>
            <!--/.Card-->

        </div>
        <!--Grid column-->


        <!--Grid row-->
        <div class="row wow fadeIn">

            <!--Grid column-->
            <div class="col-md-6 mb-4">

            </div>
            <!--Grid column-->

            <!--Grid column-->
            <div class="col-md-6 mb-4">

                <!--Card-->
                <div class="card">

                </div>
                <!--/.Card-->

            </div>
            <!--Grid column-->

        </div>
        <!--Grid row-->
    </div>


<script type="text/template" class="items-list-template">
    <td><span class="itemName"><%= itemName %></span></td>
    <td><span class="itemPrice"><%= itemPrice %></span></td>
    <td><span class="itemDescription"><%= itemDescription %></span></td>
    <td><span class="priorityLevel"><%= priorityLevel %></span></td>
    <td><span class="occasion"><%= occasion %></span></td>
    <td><span class="itemUrl d-inline-block text-truncate" style="max-width: 150px;"><%= itemUrl %></span></td>
</script>
<!--Main layout-->


<script type="text/javascript" src="<?php echo base_url('application/views/js/jquery-3.3.1.min.js'); ?>"></script>
<!-- Bootstrap tooltips -->
<script type="text/javascript" src="<?php echo base_url('application/views/js/popper.min.js'); ?>"></script>
<!-- Bootstrap core JavaScript -->
<script type="text/javascript" src="<?php echo base_url('application/views/js/bootstrap.min.js'); ?>"></script>
<!-- MDB core JavaScript -->
<script type="text/javascript" src="<?php echo base_url('application/views/js/underscore-min.js'); ?>"></script>

<script type="text/javascript" src="<?php echo base_url('application/views/js/backbone-min.js'); ?>"></script>

<script type="text/javascript" src="<?php echo base_url('application/views/js/jquery.cookie.js'); ?>"></script>

<script type="text/javascript" src="<?php echo base_url('application/views/js/share-scripts.js'); ?>"></script>

