<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css">
<!-- Bootstrap core CSS -->
<link href="<?php echo base_url('application/views/css/bootstrap.min.css'); ?>" rel="stylesheet">
<!-- Material Design Bootstrap -->
<link href="<?php echo base_url('application/views/css/mdb.min.css'); ?>" rel="stylesheet">
<!-- Your custom styles (optional) -->
<link href="<?php echo base_url('application/views/css/style.css'); ?>" rel="stylesheet">


<!--Modal Form Login with Avatar Demo-->
<div style="margin-top: 15em" class="modal-dialog cascading-modal modal-avatar modal-sm fadeIn" role="document">
    <!--Content-->
    <div class="modal-content">

        <!--Header-->
        <div class="modal-header">
            <img src="http://www.nwsecurityandsound.biz/wp-content/uploads/2017/02/male-avatar-1.png"
                 class="rounded-circle img-responsive"
                 alt="Avatar photo">
        </div>
        <!--Body-->
        <div class="modal-body text-center mb-1">

            <h5 class="mt-1 mb-2"></h5>
            <?php echo form_open('User_controller/login'); ?>
                <div class="md-form ml-0 mr-0">

                    <input type="text" name="userName" class="form-control ml-0">
                    <label class="ml-0">Enter Username</label>
                    <span class="text-danger"> <?php echo form_error('userName')?> </span>

                </div>
                <div class="md-form ml-0 mr-0">

                    <input type="password" name="password" class="form-control ml-0">
                    <label for="form1" class="ml-0">Enter password</label>
                    <span class="text-danger"> <?php echo form_error('password')?> </span>

                </div>

                <div class=" text-center mt-4">
                    <button type="submit" name="login" class="btn btn-cyan">Login<i class="fas fa-sign-in-alt ml-1"></i></button>
                </div>
                     <span class="text-danger"> <?php echo $this->session->flashdata("error"); ?> </span>

            </form>

        </div>

    </div>
    <!--/.Content-->

</div>


<div class="container ">
    <div class="row">
        <div class="col">
        </div>
        <div class="col">
            <p style="font-size: 16px">If you dont have an account <a href="" data-toggle="modal"data-target="#register">click here to Sign Up</a></p>
        </div>
        <div class="col">
        </div>
    </div>
</div>


<!--Modal: Login / Register Form Demo-->
<div class="modal fade" id="register" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
     aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Please Sign Up</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <!-- Default form register -->
                    <?php echo form_open('User_controller/register'); ?>
                    <div class="form-row mb-4">
                        <!-- First name -->
                        <input type="text" name="userName" class="form-control mb-4"
                               placeholder="User name" required>

                        <!-- E-mail -->
                        <input type="email" name="email" class="form-control mb-4"
                               placeholder="E-mail" required>


                        <!-- Password -->
                        <input type="password" name="password" class="form-control"
                               placeholder="Password" aria-describedby="defaultRegisterFormPasswordHelpBlock" required>

                        <small id="defaultRegisterFormPasswordHelpBlock" class="form-text text-muted mb-4">
                            At least 8 characters and 1 digit
                        </small>

                        <!--Add List-->
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <span class="input-group-text" id="">Wish List</span>
                            </div>
                            <input name="wishListName" type="text" class="form-control" placeholder="Name" required>
                            <input name="wishListDes" type="text" class="form-control" placeholder="Description" required>
                        </div>


                        <!-- Sign up button -->
                        <button class="btn btn-primary my-4 btn-block" type="submit">Sign Up</button>


                </form>
                <!-- Default form register -->
            </div>
        </div>
    </div>
</div>

<!--Modal: Login / Register Form Demo-->


<!--Modal Form Login with Avatar Demo-->


<!-- JQuery -->

<script type="text/javascript" src="<?php echo base_url('application/views/js/jquery-3.3.1.min.js'); ?>"></script>
<!-- Bootstrap tooltips -->
<script type="text/javascript" src="<?php echo base_url('application/views/js/popper.min.js'); ?>"></script>
<!-- Bootstrap core JavaScript -->
<script type="text/javascript" src="<?php echo base_url('application/views/js/bootstrap.min.js'); ?>"></script>
<!-- MDB core JavaScript -->
<script type="text/javascript" src="<?php echo base_url('application/views/js/mdb.min.js'); ?>"></script>

<!-- Initializations -->
<script type="text/javascript">
    // Animations initialization
    new WOW().init();

</script>
