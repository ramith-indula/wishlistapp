<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>Wish List</title>
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css">
    <!-- Bootstrap core CSS -->
    <link href="<?php echo base_url('application/views/css/bootstrap.min.css'); ?>" rel="stylesheet">
    <!-- Material Design Bootstrap -->
    <link href="<?php echo base_url('application/views/css/mdb.min.css'); ?>" rel="stylesheet">
    <!-- Your custom styles (optional) -->
    <link href="<?php echo base_url('application/views/css/style.css'); ?>" rel="stylesheet">

    <script src="https://cdnjs.cloudflare.com/ajax/libs/clipboard.js/2.0.0/clipboard.min.js"></script>
    <style>

        .map-container {
            overflow: hidden;
            padding-bottom: 56.25%;
            position: relative;
            height: 0;
        }

        .map-container iframe {
            left: 0;
            top: 0;
            height: 100%;
            width: 100%;
            position: absolute;
        }
    </style>
</head>

<body class="grey lighten-3">

<!--Main Navigation-->
<header>

    <!-- Sidebar -->
    <div class="sidebar-fixed position-fixed">

        <a class="logo-wrapper waves-effect">
            <img src="http://www.nwsecurityandsound.biz/wp-content/uploads/2017/02/male-avatar-1.png" class="img-fluid"
                 alt="">
            <hr>
            <h5 id="userName" style="text-align: center">User Name</h5>
        </a>
        <div class="list-group list-group-flush">
            <a href="#" class="list-group-item active waves-effect">
                <i class="fas  fa-clipboard-list mr-3"></i>Wish List
            </a>
            <a id="shareWishList" href="#" class="list-group-item list-group-item-action waves-effect" data-toggle="modal"
               data-target="#shareLinkModal" data-backdrop="false">
                <i class="fas fa-share-alt mr-3"></i>Share My Wish List</a>
            <a href="#" class="list-group-item list-group-item-action waves-effect">
                <i class="fas fa-user mr-3"></i>Profile</a>
            <a href="http://localhost/TheWishList2/User_controller/logout" class="list-group-item list-group-item-action waves-effect">
                <i class="fas  fa-sign-out-alt mr-3"></i>Log Out</a>
        </div>

    </div>
    <!-- Sidebar -->

</header>
<!--Main Navigation-->

<!--Main layout-->
<main class="pt-5 mx-lg-5">
    <div class="container-fluid mt-5">

        <!-- Heading -->
        <div class="card mb-4 wow fadeIn">

            <!--Card content-->
            <div class="card-body d-sm-flex justify-content-between">

                <h4 class="mb-2 mb-sm-0 pt-1">
                    <a id="wishlistname"  target="_blank">MY WISH LIST </a>
                </h4>

                <form class="d-flex justify-content-center">

                </form>
                <div class="d-flex justify-content-center">
                    <button class="btn btn-primary btn-sm my-0 p btn-additem" data-toggle="modal"
                            data-target="#AddItemModal" data-backdrop="false"> Add Item
                    </button>

                </div>

            </div>

        </div>
        <!-- Heading -->

        <!--Grid row-->

        <!--Grid column-->
        <div class="card mb-4 wow fadeIn table-items" style="display: none">

            <!--Card-->
            <div class="card-body d-sm-flex justify-content-between">

                <!--Card content-->
                <div class="card-body">

                    <!-- Table  -->
                    <table class="table table-hover">
                        <!-- Table head -->
                        <thead class="blue-grey lighten-4">
                        <tr>
                            <th>Item Name</th>
                            <th>Price</th>
                            <th>Description</th>
                            <th>Priority Level</th>
                            <th>Occasion</th>
                            <th>URL</th>
                            <th style="text-align: center">Edit</th>
                            <th style="text-align: center">Delete</th>
                        </tr>
                        </thead>
                        <!-- Table head -->

                        <!-- Table body -->
                        <tbody class="items-list"></tbody>
                        <!-- Table body -->
                    </table>
                    <!-- Table  -->

                </div>

            </div>
            <!--/.Card-->

        </div>
        <!--Grid column-->

            <div class="card-body alert alert-danger create-list" role="alert"  style="display: none">
                <p style="text-align: center">YOU DON'T HAVE ITEMS YET...</p>
            </div>


        <!--Grid row-->
        <div class="row wow fadeIn">

            <!--Grid column-->
            <div class="col-md-6 mb-4">

            </div>
            <!--Grid column-->

            <!--Grid column-->
            <div class="col-md-6 mb-4">

                <!--Card-->
                <div class="card">

                    <!--Section: Modals-->
                    <section>
                        <!--Modal: create a wish List-->
                        <div class="modal fade" id="createListModal" tabindex="-1" role="dialog"
                             aria-labelledby="myModalLabel"
                             aria-hidden="true">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title" id="exampleModalLabel">Create a new list</h5>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <div class="modal-body">
                                        <input class="form-control listName-input" placeholder="List name">
                                        <input class="form-control listDescription-input"
                                               placeholder="List Description">
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-primary add-list" data-dismiss="modal">
                                            Create list
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--Modal: create a wish List-->

                        <!-- Add Item Modal-->
                        <div class="modal fade" id="AddItemModal" tabindex="-1" role="dialog"
                             aria-labelledby="myModalLabel"
                             aria-hidden="true">
                            <div class="modal-dialog modal-lg modal-notify modal-info" role="document">
                                <!--Content-->
                                <div class="modal-content">
                                    <!--Header-->
                                    <div class="modal-header">
                                        <p class="heading lead">Add an Item</p>

                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true" class="white-text">&times;</span>
                                        </button>
                                    </div>

                                    <!--Body-->
                                    <div class="modal-body">
                                        <div class="text-center">
                                            <table class="table">
                                                <thead>
                                                <tr>
                                                    <td><input class="form-control itemName-input"
                                                               placeholder="Item name" required></td>
                                                    <td><input class="form-control itemPrice-input" placeholder="Price" required>
                                                    </td>
                                                    <td><input class="form-control itemDescription-input"
                                                               placeholder="Description" required></td>


                                                    <td>
                                                        <select class="form-control priorityLevel-input" required>
                                                            <option value="" disabled selected>Priority&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</option>
                                                            <option value="High">High</option>
                                                            <option value="Medium">Medium</option>
                                                            <option value="Low">Low</option>
                                                        </select>
                                                    </td>
                                                    <td><input class="form-control occasion-input"
                                                               placeholder="Occasion" required></td>
                                                    <td><input class="form-control itemUrl-input" placeholder="URL" required>
                                                    </td>
                                                </tr>
                                                </thead>
                                            </table>
                                        </div>

                                    </div>

                                    <!--Footer-->
                                    <div class="modal-footer">
                                        <button  class="btn btn-outline-info waves-effect add-item" data-dismiss="modal">
                                            Add
                                        </button>
                                        <!--<a role="button" class="btn btn-info">Get it now
                                            <i class="far fa-gem ml-1"></i>
                                        </a>-->

                                        <a role="button" class="btn btn-outline-info waves-effect" data-dismiss="modal">Cancel</a>
                                    </div>
                                </div>
                                <!--/.Content-->
                            </div>
                        </div>
                        <!-- Add Item Modal-->
                    </section>
                    <!--Section: Modals-->
                </div>
                <!--/.Card-->

            </div>
            <!--Grid column-->

        </div>
        <!--Grid row-->

    </div>

    <!-- Side Modal Bottom Left Warning-->
    <div class="modal fade left" id="shareLinkModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
         aria-hidden="true" data-backdrop="false">
        <div class="modal-dialog modal-side modal-bottom-left modal-notify modal-warning" role="document">
            <!--Content-->
            <div class="modal-content">
                <!--Header-->
                <div class="modal-header">
                    <p class="heading"></p>

                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true" class="white-text">&times;</span>
                    </button>
                </div>

                <!--Body-->
                <div class="modal-body">

                    <div class="row">
                        <div class="col-3 text-center">
                            <i class="fas fa-check fa-4x mb-3 animated rotateIn"></i>
                        </div>

                        <div class="col-9">
                            <p class="card-text">
                                <strong>You'r Link has been created</strong>
                            </p>
                            <p id="shareLink" class="listUrl d-inline-block text-truncate" style="max-width: 250px;">
                                NO LINK...!
                            </p>
                        </div>
                    </div>


                </div>

                <!--Footer-->
                <div class="modal-footer justify-content-center">
                    <a  role="button" class="btn btn-warning copyLink" data-clipboard-target="#shareLink">Copy Link
                        <i class="far fa-copy ml-1"></i>
                    </a>
                    <a role="button" class="btn btn-outline-warning waves-effect" data-dismiss="modal">No,
                        thanks</a>
                </div>
            </div>
            <!--/.Content-->
        </div>
    </div>
    <!-- Side Modal Bottom Left Warning-->
</main>
<!--Main layout-->

<!-- SCRIPTS -->

<!-- JQuery -->
<script type="text/javascript" src="<?php echo base_url('application/views/js/jquery-3.3.1.min.js'); ?>"></script>
<!-- Bootstrap tooltips -->
<script type="text/javascript" src="<?php echo base_url('application/views/js/popper.min.js'); ?>"></script>
<!-- Bootstrap core JavaScript -->
<script type="text/javascript" src="<?php echo base_url('application/views/js/bootstrap.min.js'); ?>"></script>
<!-- MDB core JavaScript -->
<script type="text/javascript" src="<?php echo base_url('application/views/js/underscore-min.js'); ?>"></script>
<script type="text/javascript" src="<?php echo base_url('application/views/js/backbone-min.js'); ?>"></script>
<script type="text/javascript" src="<?php echo base_url('application/views/js/jquery.cookie.js'); ?>"></script>
<script type="text/javascript" src="<?php echo base_url('application/views/js/mdb.min.js'); ?>"></script>
<script type="text/javascript" src="<?php echo base_url('application/views/js/scripts.js'); ?>"></script>

<!-- Initializations -->
<script type="text/javascript">
    // Animations initialization
    new WOW().init();

</script>

<script type="text/template" class="items-list-template">
    <td><span class="itemName"><%= itemName %></span></td>
    <td><span class="itemPrice"><%= itemPrice %></span></td>
    <td><span class="itemDescription"><%= itemDescription %></span></td>
    <td><span class="priorityLevel"><%= priorityLevel %></span></td>
    <td><span class="occasion"><%= occasion %></span></td>
    <td><span class="itemUrl d-inline-block text-truncate" style="max-width: 150px;"><%= itemUrl %></span></td>
    <td style="text-align: center">
        <button class="btn btn-warning  btn-sm my-0 p edit-item">Edit</button>
        <button class="btn btn-success  btn-sm my-0 p update-item" style="display:none">Update</button>
    </td>
    <td style="text-align: center">
        <button class="btn btn-danger  btn-sm my-0 p delete-item">Delete</button>
        <button class="btn btn-danger  btn-sm my-0 p cancel" style="display:none">Cancel</button>
    </td>

</script>



</body>
</html>
