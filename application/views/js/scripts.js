// Backbone Model
var Item = Backbone.Model.extend({
    urlRoot: 'http://localhost/TheWishList2/Item_controller/item',
    defaults: {
        itemName: '',
        itemPrice: '',
        itemDescription: '',
        priorityLevel: '',
        occasion: '',
        itemUrl: ''
    }



});

// Backbone Collection
var Items = Backbone.Collection.extend({
    url: 'http://localhost/TheWishList2/Item_controller/items',
    model: Item,
    comparator: function (item) {
        switch (item.get('priorityLevel')) {
            case 'High':
                return 1;
            case 'Medium':
                return 2;
            case 'Low':
                return 3;
            default:
                return Infinity;
        }
    }
});

// instantiate a Collection
var items = new Items();

// Backbone View for one item
var ItemView = Backbone.View.extend({
    model: new Item(),
    tagName: 'tr',
    initialize: function () {
        this.template = _.template($('.items-list-template').html());
    },

    events: {
        'click .edit-item': 'edit',
        'click .update-item': 'update',
        'click .cancel': 'cancel',
        'click .delete-item': 'delete'
    },
    edit: function () {
        $('.edit-item').hide();
        $('.delete-item').hide();
        this.$('.update-item').show();
        this.$('.cancel').show();


        var itemName = this.$('.itemName').html();
        var itemPrice = this.$('.itemPrice').html();
        var itemDescription = this.$('.itemDescription').html();
        var priorityLevel = this.$('.priorityLevel').html();
        var occasion = this.$('.occasion').html();
        var itemUrl = this.$('.itemUrl').html();


        this.$('.itemName').html('<input type="text" class="form-control itemName-update" value="' + itemName + '">');
        this.$('.itemPrice').html('<input type="text" class="form-control itemPrice-update" value="' + itemPrice + '">');
        this.$('.itemDescription').html('<input type="text" class="form-control itemDescription-update" value="' + itemDescription + '">');
        this.$('.priorityLevel').html('<select class="form-control priorityLevel-update"  ' + priorityLevel + '>' +
            ' <option value="" disabled >Priority</option>\n' +
            '        <option value="High">High</option>\n' +
            '            <option value="Medium">Medium</option>\n' +
            '            <option value="Low">Low</option>');
        this.$('.occasion').html('<input type="text" class="form-control occasion-update" value="' + occasion + '">');
        this.$('.itemUrl').html('<input type="text" class="form-control itemUrl-update" value="' + itemUrl + '">');
    },
    update: function () {

        this.model.set('itemName', $('.itemName-update').val());
        this.model.set('itemPrice', $('.itemPrice-update').val());
        this.model.set('itemDescription', $('.itemDescription-update').val());
        this.model.set('priorityLevel', $('.priorityLevel-update').val());
        this.model.set('occasion', $('.occasion-update').val());
        this.model.set('itemUrl', $('.itemUrl-update').val());
        this.model.save(null, {
            success: function (response) {
                console.log('Successfully UPDATED item with _id: ' + response.toJSON().id);
            },
            error: function (err) {
                console.log('Failed to update item!');
            }
        });
    },
    cancel: function () {
        itemsView.render();
    },
    delete: function () {
        var self = this;
        this.model.destroy({
            success: function (response) {
                items.fetch();
                console.log('Successfully DELETED item with _id: ' + response.toJSON().id);
            },
            error: function (err) {
                console.log('Failed to delete item!');
            }
        });
    },
    render: function () {
        this.$el.html(this.template(this.model.toJSON()));
        return this;
    }
});

// Backbone View for all items
var ItemsView = Backbone.View.extend({
    model: items,
    el: $('.items-list'),
    initialize: function () {
        var self = this;
        this.model.on('add', this.render, this);
        this.model.on('change', function () {
            setTimeout(function () {
                self.render();
            }, 30);
        }, this);
        this.model.on('remove', this.render, this);

        this.model.fetch({
            data: {listId: $.cookie('wishlistId')},
            success: function (response) {
                _.each(response.toJSON(), function (item) {
                    console.log('Successfully GOT item with _id: ' + item);
                })
            },
            error: function () {
                $('.table-items').hide();
                $('.create-list').show();
                console.log('Failed to get item!');
            }
        });
    },
    render: function () {
        $('.table-items').show();
        $('.create-list').hide();
        this.model.sort();
        var self = this;
        this.$el.html('');
        _.each(this.model.toArray(), function (item) {
            self.$el.append((new ItemView({model: item})).render().$el);
        });
        return this;
    }
});
var itemsView;

$(document).ready(function () {
    $('#wishlistname').html($.cookie("wishlistName"));
    $('#userName').html($.cookie("userName"));

    $('.add-item').on('click', function () {

        var item = new Item({
            listId:$.cookie("wishlistId"),
            itemName: $('.itemName-input').val(),
            itemPrice: $('.itemPrice-input').val(),
            itemDescription: $('.itemDescription-input').val(),
            priorityLevel: $('.priorityLevel-input').val(),
            occasion: $('.occasion-input').val(),
            itemUrl: $('.itemUrl-input').val()
        });
        $('.itemName-input').val('');
        $('.itemPrice-input').val('');
        $('.itemDescription-input').val('');
        $('.priorityLevel-input').val('');
        $('.occasion-input').val('');
        $('.itemUrl-input').val('');


        console.log(item.toJSON());
        item.save(null, {
                success: function (response) {
                    item.set('id', response.id);
                    items.fetch({data: {listId: $.cookie('wishlistId')}});
                    console.log('Successfully SAVED : ' + response.toJSON().id);

                },
                error: function () {
                    console.log('Failed to save');
                }
            }
        );
    });

});

//WISH LIST
//Backbone Model
var WishList = Backbone.Model.extend({
    urlRoot: 'http://localhost/TheWishList2/Item_controller/list',
    defaults: {
        listName: '',
        listDescription: '',
        userId: ''
    }
});

//Backbone Collection
var WishLists = Backbone.Collection.extend({
    url: 'http://localhost/TheWishList2/Item_controller/list',
    model: WishList
});
var wishlists = new WishLists();

//Backbone View
var WishListsView = Backbone.View.extend({
    model: wishlists,
    initialize: function () {
        var userId = $.cookie('userId');
        this.model.fetch({
            data: {userId},
            success: function (response) {
                if (response.toJSON()[0].list) {
                    itemsView = new ItemsView();
                    $('.table-items').show();
                    $('.btn-creatList').hide();
                } else {
                    $('.table-items').hide();
                    $('.btn-order').hide();
                    $('.btn-additem').hide();
                    $('.create-list').show();

                }
            },
            error: function () {
                console.log('Failed to get List!');
            }
        });

    }
});

var wishlistview = new WishListsView();

$(document).ready(function () {
    var clipboard = new ClipboardJS('.copyLink');
    $('.add-list').on('click', function () {
        var wishlist = new WishList({
            listName: $('.listName-input').val(),
            listDescription: $('.listDescription-input').val(),
        });
        $('.listName-input').val('');
        $('.listDescription-input').val('');

        wishlistview.render();

        //items.add(item);
        console.log(wishlist.toJSON());
        wishlist.save(null, {
                success: function (response) {

                    console.log('Successfully SAVED : ' + response.toJSON().id);

                },
                error: function () {
                    console.log('Failed to save');
                }
            }
        );
    });
    $('#shareWishList').on('click', function () {
        $('#shareLink').html("http://localhost/TheWishList2/share/" + $.cookie('wishlistId')  );
    });

});