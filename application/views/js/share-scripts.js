// Backbone Model
var Item = Backbone.Model.extend({
    urlRoot: 'http://localhost/TheWishList2/Item_controller/item',
    defaults: {
        itemName: '',
        itemPrice: '',
        itemDescription: '',
        priorityLevel: '',
        occasion: '',
        itemUrl: ''
    }


});

// Backbone Collection
var Items = Backbone.Collection.extend({
    url: 'http://localhost/TheWishList2/Item_controller/items',
    model: Item,
    comparator: function (item) {
        switch (item.get('priorityLevel')) {
            case 'High':
                return 1;
            case 'Medium':
                return 2;
            case 'Low':
                return 3;
            default:
                return Infinity;
        }
    }
});

// instantiate a Collection
var items = new Items();
var listId;
// Backbone View for one item
var ItemView = Backbone.View.extend({
    model: new Item(),
    tagName: 'tr',
    initialize: function () {
        this.template = _.template($('.items-list-template').html());
    },

    render: function () {
        this.$el.html(this.template(this.model.toJSON()));
        return this;
    }
});

// Backbone View for all items
var ItemsView = Backbone.View.extend({
    model: items,
    el: $('.items-list'),
    initialize: function () {
        var self = this;
        this.model.on('add', this.render, this);
        this.model.on('change', function () {
            setTimeout(function () {
                self.render();
            }, 30);
        }, this);
        this.model.on('remove', this.render, this);

        this.model.fetch({
            data: {listId: listId},
            //$.cookie('wishlistId')
            success: function (response) {
                _.each(response.toJSON(), function (item) {
                    console.log('Successfully GOT item with _id: ' + item);
                })
            },
            error: function () {
                $('.table-items').hide();
                $('.create-list').show();
                console.log('Failed to get item!');
            }
        });
    },
    render: function () {
        $('.table-items').show();
        $('.create-list').hide();
        this.model.sort();
        var self = this;
        this.$el.html('');
        _.each(this.model.toArray(), function (item) {
            self.$el.append((new ItemView({model: item})).render().$el);
        });
        return this;
    }
});


function getListID() {
    var url = window.location.href;
    var array = url.split("/");
    listId = array[array.length - 1];
    var itemsView = new ItemsView();
}


$(document).ready(function () {
    $('#wishlistname').html($.cookie("wishlistName"));
    getListID();


});








