<?php

class User_controller extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('User_model');
    }


    public function login()
    {
        $this->load->library('form_validation');
        $this->form_validation->set_rules('userName', 'UserName', 'required');
        $this->form_validation->set_rules('password', 'Password', 'required');

        if ($this->form_validation->run()) {

            $username = $this->input->post('userName');
            $password = $this->input->post('password');
            $this->load->model('User_model');
            if ($data['user'] = $this->User_model->can_login($username, $password)) {


                $userData = array("userId"=>$data['user'][0]['userId']);
                $wishList=$this->User_model->getListIdOfUser($userData);

                $cookie_name = "userId";
                $cookie_value = $data['user'][0]['userId'];
                setcookie($cookie_name, $cookie_value, time() + (86400 * 30), "/");

                $cookie_name = "userName";
                $cookie_value = $data['user'][0]['userName'];
                setcookie($cookie_name, $cookie_value, time() + (86400 * 30), "/");

                $cookie_name = "wishlistId";
                $cookie_value =$wishList[0]['id'];
                setcookie($cookie_name, $cookie_value, time() + (86400 * 30), "/"); // 86400 = 1 day

                $cookie_name = "wishlistName";
                $cookie_value =$wishList[0]['listName'];
                setcookie($cookie_name, $cookie_value, time() + (86400 * 30), "/"); // 86400 = 1 day

                redirect(base_url() . 'dashboard');
            } else {
                $this->session->set_flashdata('error', 'Invalid Username or Password');
                redirect(base_url() . 'User_controller/login');
            }
        } else {
            $data['Title'] = 'Login';
            $this->load->view('login', $data);
        }
    }

    public function dashboard()
    {
        if ($_COOKIE['userId'] != '') {
            $this->load->view('dashboard');
        } else {
            redirect(base_url() . 'User_controller/login');
        }
    }

    public function logout()
    {
        $this->session->unset_userdata('username');
        redirect(base_url() . 'login');
    }

    public function register()
    {
        $this->load->library('form_validation');
        $this->form_validation->set_rules('userName', 'Username', 'required');
        $this->form_validation->set_rules('email', 'Email', 'required');
        $this->form_validation->set_rules('password', 'Password', 'required');
        $this->form_validation->set_rules('wishListName', 'WishListName', 'required');
        $this->form_validation->set_rules('wishListDes', 'WishListDes', 'required');

        if ($this->form_validation->run()) {

            $username = $this->input->post('userName');
            $email = $this->input->post('email');
            $password = $this->input->post('password');
            $wishListName = $this->input->post('wishListName');
            $wishListDes = $this->input->post('wishListDes');
            $userId=$this->User_model->registerUser($username, $email, $password );
            $this->User_model->addWishList($userId,$wishListName,$wishListDes);
            $data = array("userId"=>$userId);
            $wishList=$this->User_model->getListIdOfUser($data);

            $cookie_name = "userId";
            $cookie_value = $userId;
            setcookie($cookie_name, $cookie_value, time() + (86400 * 30), "/");

            $cookie_name = "userName";
            $cookie_value = $data['user'][0]['userName'];
            setcookie($cookie_name, $cookie_value, time() + (86400 * 30), "/");

            $cookie_name = "wishlistId";
            $cookie_value =$wishList[0]['id'];
            setcookie($cookie_name, $cookie_value, time() + (86400 * 30), "/"); // 86400 = 1 day

            $cookie_name = "wishlistName";
            $cookie_value =$wishList[0]['listName'];
            setcookie($cookie_name, $cookie_value, time() + (86400 * 30), "/"); // 86400 = 1 day

            redirect(base_url() . 'dashboard');

        } else {

            redirect(base_url() . 'User_controller/login');
        }
    }

    public function shareList(){
        $this->load->view('shareView');
    }


}