<?php

use Restserver\Libraries\REST_Controller;

require APPPATH . 'libraries/REST_Controller.php';
require APPPATH . 'libraries/Format.php';

class Item_controller extends REST_Controller
{

    public function __construct()
    {
        parent::__construct();

        $this->load->model('items_model');
        $this->load->model('User_model');
    }

    //API - get all items
    function items_get()
    {

        $listId = $this->get('listId');

        if (!$listId) {

            $this->response("No list specified", 400);

            exit;
        }

        $result = $this->items_model->getAllItems($listId);


        if ($result) {

            $this->response($result, 200);

            exit;
        } else {

            $this->response("Invalid List ID", 404);

            exit;
        }
    }

    //API - Add an item
    function item_post()
    {
        $listId = $this->post('listId');

        $itemName = $this->post('itemName');

        $itemPrice = $this->post('itemPrice');

        $itemDescription = $this->post('itemDescription');

        $priorityLevel = $this->post('priorityLevel');

        $occasion = $this->post('occasion');

        $itemUrl = $this->post('itemUrl');

        //GET THR LIST ITEM DYNAMICALLY
        //$listId = 6;

        if (!$itemName || !$itemPrice || !$itemDescription || !$priorityLevel || !$occasion || !$itemUrl || !$listId) {

            $this->response("Enter complete book information to save" . " " . $itemName . " " . $itemPrice . " " . $itemDescription . " " . $priorityLevel . " " . $itemUrl . " " . $occasion . " " . $listId, 400);

        } else {

            $result = $this->items_model->add(array("itemName" => $itemName, "itemPrice" => $itemPrice, "itemDescription" => $itemDescription, "priorityLevel" => $priorityLevel, "occasion" => $occasion, "itemUrl" => $itemUrl, "listId" => $listId));

            if (!$result) {

                $this->response("Book information coild not be saved. Try again.", 404);

            } else {
                $returnObj['id'] = $result;
                $this->response($returnObj, 200);

            }

        }

    }

    //API - Update a item
    function item_put()
    {
        $id = $this->put('id');

        $itemName = $this->put('itemName');

        $itemPrice = $this->put('itemPrice');

        $itemDescription = $this->put('itemDescription');

        $priorityLevel = $this->put('priorityLevel');

        $occasion = $this->put('occasion');

        $itemUrl = $this->put('itemUrl');

        if (!$id || !$itemPrice || !$itemName || !$itemDescription || !$priorityLevel || !$occasion || !$itemUrl) {

            $this->response("ERROR! Enter valid data." . " " . $itemName . " " . $itemPrice . " " . $itemDescription . " " . $priorityLevel . " " . $itemUrl . " " . $occasion, 400);

        } else {

            $result = $this->items_model->updateItem($id, array("itemName" => $itemName, "itemPrice" => $itemPrice, "itemDescription" => $itemDescription, "priorityLevel" => $priorityLevel, "occasion" => $occasion, "itemUrl" => $itemUrl));

            if ($result === 0) {

                $this->response("Book information coild not be saved. Try again.", 404);

            } else {

                $this->response("success", 200);

            }

        }
    }

    //API - delete an item
    function item_delete($id)
    {


        if (!$id) {
            $this->response("ID missing", 404);
        }
        if ($this->items_model->deleteItem($id)) {
            $this->response("Successfully Deleted", 200);
        } else {
            $this->response("Deleting failed", 400);

        }

    }

    //API - Add an list
    function list_post()
    {

        $listName = $this->post('listName');

        $listDescription = $this->post('listDescription');

        //$userId = $this->post('userId');

        //GET THE USER id DYNAMICALLY
        $userId = 5;

        if (!$listName || !$listDescription || !$userId) {

            $this->response("Enter complete book information to save" . "" . $listName . "" . $listDescription . "" . $userId, 400);

        } else {

            $result = $this->User_model->add(array("listName" => $listName, "listDescription" => $listDescription, "userId" => $userId));

            if ($result === 0) {

                $this->response("Book information coild not be saved. Try again.", 404);

            } else {

                $this->response("success", 200);

            }

        }

    }

    function list_get()
    {

        //$userId = 2;
        $userId = $this->get('userId');

        $result['list'] = $this->User_model->getAllItems($userId)[0];

        if ($result) {

            $this->response($result, 200);

            exit;
        } else {

            $this->response("Invalid List", 404);

            exit;
        }

    }
}